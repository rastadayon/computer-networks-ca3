import socket
import json
from time import time
from HelloMessage import HelloMessage
import traceback
import sys
from Logger import Logger
from random import randrange
from copy import deepcopy

class Peer():
    def __init__(self, id, ip, port, allPeersInfo, N):
        print("port inited with id = {}, port = {}, ip = {}".format(id, port, ip))
        self.id = id
        self.ip = ip
        self.port = port
        self.allPeersInfo = allPeersInfo
        self.N = N
        self.heardNodes = list()
        self.uniDirConnection = list()
        self.biDirConnection = list()
        self.isOff = False
        self.isTerminated = False
        self.connectionTimes = {}       # to calculate neighbor availabality
        for peer in self.allPeersInfo:
            if peer[0] == self.id:
                continue
            else:
                self.connectionTimes[peer[0]] = []
        self.sentPacketsAmount = {}       
        for peer in self.allPeersInfo:
            if peer[0] == self.id:
                continue
            else:
                self.sentPacketsAmount[peer[0]] = 0
        self.recievedPacketsAmount = deepcopy(self.sentPacketsAmount)

        self.UDPSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
        self.UDPSocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.UDPSocket.bind((self.ip, self.port))
        self.UDPSocket.setblocking(0)
        self.bufferSize = 4096

        self.lastSentHelloTimes = {i[0] : None for i in self.allPeersInfo}
        self.lastRecvHelloTimes = {i[0] : None for i in self.allPeersInfo}

        self.topologyMatrix = [[0] * len(self.allPeersInfo) for i in range(len(self.allPeersInfo))]

        self.logger = Logger(self.id)
        
        self.wantToBeNeighbour = None
        self.lastSentTimeToWantToBeNeighbour = None


    def closeSock(self):
        self.isOff = True

    def openSock(self):
        self.isOff = False

    def terminate(self):
        self.isTerminated = True
    
    def willPacketBeLost(self):
        random = randrange(100)
        if random < 5:
            return True
        else:
            return False

    def updateStatusRecv(self, recvdMessage):
        # assumed that availabality means bidirectional connection
        now = time()
        senderId = recvdMessage['senderId']
        self.recievedPacketsAmount[senderId] += 1
        self.lastRecvHelloTimes[senderId] = now
        if(senderId in self.uniDirConnection):
            if(self.id in recvdMessage['heardNodes']):
                self.uniDirConnection.remove(senderId)
                if(len(self.biDirConnection) < self.N):
                    self.biDirConnection.append(senderId)
                    self.connectionTimes[recvdMessage['senderId']].append([time()])         # open time
        elif(senderId in self.biDirConnection):
            if(self.id not in recvdMessage['heardNodes']):
                self.biDirConnection.remove(senderId)
                self.connectionTimes[recvdMessage['senderId']][-1].append(time())       # close time
                self.uniDirConnection.append(senderId)
        else:
            if(self.id in recvdMessage['heardNodes']):
                if(len(self.biDirConnection) < self.N):
                    self.biDirConnection.append(senderId)
                    self.connectionTimes[recvdMessage['senderId']].append([time()])         # open time
            else:
                self.uniDirConnection.append(senderId)

        if(senderId not in self.heardNodes):
            self.heardNodes.append(senderId)

        self.updateTopology(recvdMessage)


    def sendToRandom(self):
        wantToBeNeighbourFound = False
        if(self.wantToBeNeighbour == None or (time() - self.lastSentTimeToWantToBeNeighbour) >= 8):
            while (not wantToBeNeighbourFound):
                neighbourId = randrange(len(self.allPeersInfo))
                
                if(neighbourId == self.id or neighbourId in self.biDirConnection):
                    continue
                self.wantToBeNeighbour = neighbourId
                helloMessage = HelloMessage(self.id, self.ip, self.port, 
                    "hello", self.heardNodes, self.biDirConnection, self.lastSentHelloTimes[neighbourId], 
                    self.lastRecvHelloTimes[neighbourId])
                messageInfo = helloMessage.getJsonMessage()
                bytesToSend = str.encode(messageInfo)
                address = (self.ip, self.allPeersInfo[neighbourId][2])
                try:
                    self.UDPSocket.sendto(bytesToSend, address)
                    self.sentPacketsAmount[neighbourId] += 1
                    wantToBeNeighbourFound = True
                    self.lastSentTimeToWantToBeNeighbour = time()
                except:
                    print(traceback.print_exc())
                

    def recvHello(self):
        try:
            bytesAddressPair = self.UDPSocket.recvfrom(self.bufferSize)
            message = bytesAddressPair[0]
            messageInfo = json.loads(message.decode())
            self.updateStatusRecv(messageInfo)
        except :
            pass


    def sendToNeighbours(self, neighbours):
        for neighbour in neighbours:
            if (self.willPacketBeLost()):
                continue
            helloMessage = HelloMessage(self.id, self.ip, self.port, 
                    "hello", self.heardNodes, self.biDirConnection, self.lastSentHelloTimes[neighbour], 
                    self.lastRecvHelloTimes[neighbour])
            messageInfo = helloMessage.getJsonMessage()
            bytesToSend = str.encode(messageInfo)
            address = (self.ip, self.allPeersInfo[neighbour][2])
            try:
                self.UDPSocket.sendto(bytesToSend, address)
                self.sentPacketsAmount[neighbour] += 1
            except:
                print(traceback.print_exc())


    def updateStatus(self):
        now = time()
        for i in range(len(self.lastRecvHelloTimes)):
            then = self.lastRecvHelloTimes[i]
            if then != None and now - then > 8:
                if(i in self.biDirConnection):
                    self.biDirConnection.remove(i)
                    self.connectionTimes[i][-1].append(time())
                if(i in self.uniDirConnection):
                    self.uniDirConnection.remove(i)
                if(i in self.heardNodes):
                    self.heardNodes.remove(i)


    def updateTopology(self, recvdMessage):
        senderId = recvdMessage['senderId']
        sendersNeighbours = recvdMessage['neighbourNodes']

        # clear rows and column related to sender and self nodes
        self.topologyMatrix[senderId] = [0] * len(self.allPeersInfo)
        self.topologyMatrix[self.id] = [0] * len(self.allPeersInfo)
        for row in self.topologyMatrix:
            for i in range(len(row)):
                if i == self.id or i == senderId:
                    row[i] = 0

        for neighbour in sendersNeighbours:
            self.topologyMatrix[senderId][neighbour] = 2
            self.topologyMatrix[neighbour][senderId] = 2
        for neighbour in self.biDirConnection:
            self.topologyMatrix[self.id][neighbour] = 2
            self.topologyMatrix[neighbour][self.id] = 2
        for neighbour in self.uniDirConnection:
            self.topologyMatrix[self.id][neighbour] = 1


    def start(self):
        then = time()
        startTime = time()
        lastLogTime = 0
        logPeriod = 2
        print("start function of socket id {} called".format(self.id))
        while True:
            # logging
            if (lastLogTime == 0 or time() - lastLogTime >= logPeriod):
                lastLogTime = time()
                self.logger.logAvailability(self.connectionTimes, startTime)
                self.logger.logConnections(self.allPeersInfo, self.sentPacketsAmount, self.recievedPacketsAmount, startTime)
                self.logger.logValidNeighbours(self.biDirConnection, startTime)
                self.logger.logTopology(self.topologyMatrix, startTime)

            if (self.isTerminated):
                break
            if (self.isOff):
                continue
            try:
                self.recvHello()
                if(len(self.biDirConnection) < self.N):
                    self.sendToRandom()
                now = time()
                if(now - then >= 2):
                    self.sendToNeighbours(self.biDirConnection)
                    self.sendToNeighbours(self.uniDirConnection)
                    then = now
                self.updateStatus()
            except:
                print(traceback.print_exc())
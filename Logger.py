import json
import os
from time import time

class Logger():
    def __init__(self, id):
        self.id = id
        self.folderName = 'logs/logs_' + str(self.id) + '/'
        self.connectionLogFilePath = self.folderName + 'connection-log.txt'
        self.topologyLogPath = self.folderName + 'topology-log.txt'
        self.validNeighbourLogPath = self.folderName + 'valid-neighbour-log.txt'
        self.availabilityPath = self.folderName + 'availability-log.txt'

        self.resetFile(self.connectionLogFilePath)
        self.resetFile(self.topologyLogPath)
        self.resetFile(self.validNeighbourLogPath)
        self.resetFile(self.availabilityPath)


    def resetFile(self, filePath):
        with open(filePath, 'w') as f:
            f.write('')


    def logConnections(self, allPeersInfo, sentAmount, recievedAmount, startTime):
        outp = []
        curTime = time()
        for peer in allPeersInfo:
            if peer[0] == self.id:
                continue
            if (sentAmount[peer[0]] == 0 and recievedAmount[peer[0]] == 0):
                continue
            log = 'Address: ' + str(peer[1]) + ':' + str(peer[2]) + ', '
            log += 'Sent Packets: ' + str(sentAmount[peer[0]]) + ', '
            log += 'Recieved Packets: ' + str(recievedAmount[peer[0]])
            outp.append(log)
        with open(self.connectionLogFilePath, 'a+') as f:
            f.write('Time: ' + str(int(curTime - startTime)) + '\n')
            f.write(str(outp) + '\n----------------------------------------------------------\n')


    def logTopology(self, topology, startTime):
        curTime = time()
        with open(self.topologyLogPath, 'a+') as f:
            f.write('Time: ' + str(int(curTime - startTime)) + '\n')
            f.write(str(topology) + '\n----------------------------------------------------------\n')


    def logValidNeighbours(self, validNeighbourInfo, startTime):
        curTime = time()
        with open(self.validNeighbourLogPath, 'a+') as f:
            f.write('Time: ' + str(int(curTime - startTime)) + '\n')
            f.write(str(validNeighbourInfo) + '\n----------------------------------------------------------\n')


    def logAvailability(self, connectionTimes, startTime):
        availabilitys = {}
        curTime = time()
        for i in range(len(connectionTimes)+1):
            if i == self.id:
                continue
            totalTime = 0
            for period in connectionTimes[i]:
                if (len(period) == 1):
                    totalTime += curTime - period[0]
                elif (len(period) == 2):
                    totalTime += period[1] - period[0]
            availabilitys[i] = str(int(totalTime / (curTime - startTime) * 100)) + '%'
        with open(self.availabilityPath, 'a+') as f:
            f.write('Time: ' + str(int(curTime - startTime)) + '\n')
            f.write(str(availabilitys) + '\n----------------------------------------------------------\n')
import socket
from Peer import Peer
from _thread import start_new_thread
from time import time
from time import sleep
from random import randrange

totalNodeNum = 6
N = 3
localIP = "127.0.0.1"
localPort = 9000

totalPeers = []
allPeersInfo = []

for i in range(totalNodeNum):
    allPeersInfo.append([i, localIP, localPort + 10*i])

for i in range(totalNodeNum):
    totalPeers.append(Peer(i, localIP, localPort + 10*i, allPeersInfo, N))

for i in range(len(totalPeers)):
    try:
        start_new_thread(totalPeers[i].start, ())
    except:
        pass

then = time()
closedNodes = []
closedNodesTimes = []
lastTimeClose = time()
while True:
    if (time() - then >= 60*5):
        break

    if (len(closedNodesTimes) > 0 and time() - closedNodesTimes[0] >= 20):
        totalPeers[closedNodes[0]].openSock()  
        closedNodes.pop(0)
        closedNodesTimes.pop(0)

    while (time() - lastTimeClose >= 10):
        nodeNum = randrange(totalNodeNum)
        if nodeNum not in closedNodes:
            closedNodes.append(nodeNum)
            closedNodesTimes.append(time())
            totalPeers[nodeNum].closeSock()
            lastTimeClose = time()
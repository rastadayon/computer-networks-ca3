import json

class HelloMessage():
    def __init__(self, senderId, senderIp, senderPort, packetType, heardNodes, neighbourNodes, lastSentTime, lastRecievedTime):
        ## the fuck is packet type? 
        ## what is neighbors supposed to be? the heard nodes or the bidirectional ones. 
        ## ill assume it's the heard nodes cuz otherwise it's bullshit
         
        self.senderId = senderId
        self.senderIp = senderIp
        self.senderPort = senderPort
        self.packetType = packetType
        self.heardNodes = heardNodes # list of id
        self.lastSentTime = lastSentTime
        self.lastRecievedTime = lastRecievedTime
        self.neighbourNodes = neighbourNodes

        self.jsonDict = {'senderId': self.senderId, 'senderIp' : self.senderIp, 
                        'senderPort': self.senderPort, 'packetType' : self.packetType,
                        'heardNodes': self.heardNodes, 'neighbourNodes' : neighbourNodes,
                        'lastSentTime': self.lastSentTime, 'lastRecievedTime' : self.lastRecievedTime}

    def getJsonMessage(self):
        return(json.dumps(self.jsonDict))

